**EMMC driver developed for STM32L496 based custom board at Gadgeon Smart Systems,Kochi**

This consists of the following 
1. EMMC driver standalone
2. EMMC driver based on freeRTOS
3. USB MSC device based upon the following EMMC standalone driver
4. USB MSC device based upon the following EMMC driver based upon freeRTOS as well.

*Switch to the appropriate branches to view contents*

---

## EMMC used IS21ES04G

Refer the eMMC datasheet and JEDEC 5.0 to understand the initilization sequences used at the driver level.

All changes made by **Gadgeon Smart Systems** has been identified at places using comments.


---

## Development Overview

The driver developed is based on the following exsisting resources.

1. STM32Cube (v4.26.0)
2. FatFs - Generic FAT file system module  R0.12c 
3. FreeRTOS V9.0.0
4. IAR Workbench v8.22


---

## Contributors:

Developers involved.

1. Ani P C
2. Antish Antony
3. Deon Davis
4. Dijo Paulson
5. Navas K K

[Gadgeon Smart Systems](https://www.gadgeon.com/)